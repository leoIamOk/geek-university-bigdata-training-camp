import com.google.common.collect.Lists;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.NamespaceDescriptor;
import org.apache.hadoop.hbase.NamespaceNotFoundException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class HBaseTest {
    private static final String NAMESPACE_NAME = "leizhu";
    private static final String TABLE_NAME = "student";
    private static final String COLUMN_FAMILY_INFO = "info";
    private static final String COLUMN_INFO_STUDENT_ID = "student_id";
    private static final String COLUMN_INFO_CLASS = "class";
    private static final String COLUMN_FAMILY_SCORE = "score";
    private static final String COLUMN_SCORE_UNDERSTANDING = "understanding";
    private static final String COLUMN_SCORE_PROGRAMMING = "programming";

    private static final Connection connection;
    private static final Admin admin;
    private static final Table table;

    static {
        Configuration config = HBaseConfiguration.create();
        String path = HBaseTest.class
                .getClassLoader()
                .getResource("hbase-site.xml")
                .getPath();
        config.addResource(new Path(path));
        try {
            connection = ConnectionFactory.createConnection(config);
            admin = connection.getAdmin();
            table = createTable(NAMESPACE_NAME,TABLE_NAME);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public static void main(String [] args) throws IOException {
        insertRow("Tom", "20210000000001", "1", "75", "82");
        insertRow("Jerry", "20210000000002", "1", "85", "67");
        insertRow("Jack", "20210000000003", "2", "80", "80");
        insertRow("Rose", "20210000000004", "2", "60", "61");
        insertRow("Lei Zhu", "G20200343090051", "1", "100", "100");

        deleteRows(Lists.newArrayList("Tom", "Jerry", "Jack", "Rose"));

        dropTable(NAMESPACE_NAME, TABLE_NAME);
    }

    private static Table createTable(String namespaceStr, String tableNameStr) throws IOException {
        TableName tableName = TableName.valueOf(namespaceStr, tableNameStr);
        TableDescriptorBuilder.ModifyableTableDescriptor desc = new TableDescriptorBuilder.ModifyableTableDescriptor(tableName);
        desc.setColumnFamily(ColumnFamilyDescriptorBuilder.of(COLUMN_FAMILY_INFO));
        desc.setColumnFamily(ColumnFamilyDescriptorBuilder.of(COLUMN_FAMILY_SCORE));
        try {
            admin.getNamespaceDescriptor(namespaceStr);
        } catch(NamespaceNotFoundException nnfe) {
            admin.createNamespace(NamespaceDescriptor.create(namespaceStr).build());
        }
        if(admin.tableExists(tableName)) {
            dropTable(namespaceStr, tableNameStr);
        }
        admin.createTable(desc);
        return connection.getTable(tableName);
    }

    private static void dropTable(String namespaceStr, String tableName) throws IOException {
        admin.disableTable(TableName.valueOf(namespaceStr, tableName));
        admin.deleteTable(TableName.valueOf(namespaceStr, tableName));
    }

    private static void insertRow(String rowKey, String studentId, String classId, String understandingScore, String programmingScore) throws IOException {
        Put p = new Put(Bytes.toBytes(rowKey));
        p.addColumn(COLUMN_FAMILY_INFO.getBytes(), COLUMN_INFO_STUDENT_ID.getBytes(), Bytes.toBytes(studentId));
        p.addColumn(COLUMN_FAMILY_INFO.getBytes(), COLUMN_INFO_CLASS.getBytes(), Bytes.toBytes(classId));
        p.addColumn(COLUMN_FAMILY_SCORE.getBytes(), COLUMN_SCORE_UNDERSTANDING.getBytes(), Bytes.toBytes(understandingScore));
        p.addColumn(COLUMN_FAMILY_SCORE.getBytes(), COLUMN_SCORE_PROGRAMMING.getBytes(), Bytes.toBytes(programmingScore));
        table.put(p);
    }

    private static void deleteRows(List<String> rowKeys) throws IOException {
        table.delete(rowKeys.stream().map(rowKey -> new Delete(Bytes.toBytes(rowKey))).collect(Collectors.toList()));
    }
}
