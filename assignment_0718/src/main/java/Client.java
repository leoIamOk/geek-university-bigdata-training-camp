import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Client {
    public static void main(String [] args) {
        try {
            NameFinder proxy = RPC.getProxy(NameFinder.class, 2L, new InetSocketAddress("127.0.0.1", 12346), new Configuration());
            String studentName = proxy.findName("20210123456789");
            System.out.printf("student %s is found%n", studentName);
            studentName = proxy.findName("G20200343090051");
            System.out.printf("student %s is found%n", studentName);
            studentName = proxy.findName("20210000000000");
            System.out.printf("student %s is found%n", studentName);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
