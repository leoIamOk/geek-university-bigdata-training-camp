package sample;

import org.apache.hadoop.ipc.ProtocolSignature;

public class MyInterfaceImpl implements MyInterface {
    @Override
    public long getProtocolVersion(String s, long l) {
        return MyInterface.versionID;
    }

    @Override
    public ProtocolSignature getProtocolSignature(String s, long l, int i) {
        return new ProtocolSignature();
    }

    @Override
    public int add(int number1, int number2) {
        return number1 + number2;
    }
}
