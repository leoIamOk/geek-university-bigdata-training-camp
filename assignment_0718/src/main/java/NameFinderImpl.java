import org.apache.hadoop.ipc.ProtocolSignature;

public class NameFinderImpl implements NameFinder {
    @Override
    public long getProtocolVersion(String s, long l) {
        return NameFinder.versionID;
    }

    @Override
    public ProtocolSignature getProtocolSignature(String s, long l, int i) {
        return new ProtocolSignature();
    }

    @Override
    public String findName(String studentId) {
        if("20210123456789".equals(studentId)) {
            return "心心";
        } else if("G20200343090051".equals(studentId)) {
            return "朱磊";
        } else {
            return null;
        }
    }
}
