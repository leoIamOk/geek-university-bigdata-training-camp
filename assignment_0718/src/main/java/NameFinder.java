import org.apache.hadoop.ipc.VersionedProtocol;

public interface NameFinder extends VersionedProtocol {
    long versionID = 2L;
    String findName(String studentId);
}
