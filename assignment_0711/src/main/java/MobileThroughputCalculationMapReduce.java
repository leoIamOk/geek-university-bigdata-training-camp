import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MobileThroughputCalculationMapReduce {

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "MobileThroughputCalculationMapReduce");
    job.setMapperClass(MobileThroughputCalculationMapper.class);
    job.setReducerClass(MobileThroughputCalculationReducer.class);
    job.setJarByClass(MobileThroughputCalculationMapReduce.class);

    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(FlowBean.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(FlowBean.class);

    FileInputFormat.setInputPaths(job, new Path("hdfs:/user/student/leizhu/HTTP_20130313143750.dat"));
    FileOutputFormat.setOutputPath(job, new Path("hdfs:/user/student/leizhu/output"));
    boolean b = job.waitForCompletion(true);
    if (!b){
      throw new RuntimeException("job failed!");
    }
  }
}