import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MobileThroughputCalculationMapper extends Mapper<LongWritable, Text, Text, FlowBean> {

  @Override
  protected void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
    String line = value.toString();
    String[] words = line.split("\t");
    String mobilePhoneNumber = words[1] == null ? "" : words[1];
    long outBoundThroughput = parseLong(words[words.length - 3]);
    long inBoundThroughput = parseLong(words[words.length - 2]);
    context.write(new Text(mobilePhoneNumber), new FlowBean(outBoundThroughput, inBoundThroughput));
  }

  public static long parseLong(String s) {
    try {
      return Long.parseLong(s);
    } catch(Throwable t) {
      return -1;
    }
  }
}
