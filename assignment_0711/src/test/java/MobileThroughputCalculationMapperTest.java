import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

public class MobileThroughputCalculationMapperTest {

  @Test
  public void test() throws IOException, InterruptedException {
    MobileThroughputCalculationMapper fixture = new MobileThroughputCalculationMapper();
    fixture.map(null, new Text("1363157985066 \t13726230503\t00-FD-07-A4-72-B8:CMCC\t120.196.100.82\ti02.c.aliimg.com\t\t24\t27\t2481\t24681\t200"),
            Mockito.mock(Mapper.Context.class));
  }

}
