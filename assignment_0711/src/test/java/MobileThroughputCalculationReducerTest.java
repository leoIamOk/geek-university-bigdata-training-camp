import com.google.common.collect.Lists;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

public class MobileThroughputCalculationReducerTest {

  @Test
  public void test() throws IOException, InterruptedException {
    MobileThroughputCalculationReducer fixture = new MobileThroughputCalculationReducer();
    fixture.reduce(new Text("13726230503"), Lists.newArrayList(new FlowBean(1,2), new FlowBean(4,3), new FlowBean(8,9)),
            Mockito.mock(Reducer.Context.class));
  }

}
